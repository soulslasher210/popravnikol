export interface Student {
    id:number;
    indeks:string;
    ime:string;
    prezime:string;
    datumRodjenja:string;
}
