import { Zadatak } from './../../interface/zadatak';
import { RadService } from './../rad.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-zadatak-detalj',
  templateUrl: './zadatak-detalj.component.html',
  styleUrls: ['./zadatak-detalj.component.css']
})
export class ZadatakDetaljComponent implements OnInit {
  
  item:Zadatak={}as Zadatak;
  izmeniForm;
  
  constructor(private service:RadService,
    private route:ActivatedRoute,
    private formBuilder:FormBuilder) {
    
   }

  ngOnInit(): void {
    this.dobaviSve();
  }
  dobaviSve(){
    this.service.detailZadatak(this.route.snapshot.params['id']).subscribe(data=>{
      this.item = data;
      this.izmeniForm = this.formBuilder.group({
        id:this.item.id,
        oblast:this.item.oblast,
        predmet:this.item.predmet,
      }as Zadatak)
    });
  }


  izmeni(item){
    this.service.changeZadatak(item).subscribe((data)=>this.item=data);
  }
  

}
