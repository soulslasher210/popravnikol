import { ZadatakDetaljComponent } from './zadatak-detalj/zadatak-detalj.component';
import { ZadatakComponent } from './zadatak/zadatak.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path:"zadatak",component:ZadatakComponent
  },
  {
    path:"zadatak/zadatakDetail",component:ZadatakDetaljComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
