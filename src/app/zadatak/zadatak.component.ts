import { Router } from '@angular/router';
import { Zadatak } from './../../interface/zadatak';
import { RadService } from './../rad.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-zadatak',
  templateUrl: './zadatak.component.html',
  styleUrls: ['./zadatak.component.css'],
})
export class ZadatakComponent implements OnInit {
  lista: Zadatak[] = [];
  dodajForm;
  constructor(
    private service: RadService,
    private route: Router,
    private formBuilder: FormBuilder
  ) {
    this.dodajForm = this.formBuilder.group({
      id: 0,
      oblast: '',
      predmet: '',
    } as Zadatak);
  }

  ngOnInit(): void {
    this.zadatak();
  }

  zadatak() {
    this.service.getZadatak().subscribe((data) => (this.lista = data));
  }

  obrisi(id) {
    this.service.deleteZadatak(id).subscribe(() => this.zadatak());
  }
  dodaj(item) {
    this.service.putZadatak(item).subscribe(() => this.zadatak());
  }

  detalji(id){
    this.route.navigate(["zadatak/zadatakDetail", {id:id}]);

  }

}
