import { Zadatak } from './../interface/zadatak';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RadService {

  constructor(private http:HttpClient) { }

  adresa:string = "http://localhost:3000/";
  
  getZadatak():Observable<Zadatak[]>{
    return this.http.get<Zadatak[]>(this.adresa+"zadatak");
  }
  deleteZadatak(id):Observable<Zadatak>{
    return this.http.delete<Zadatak>(this.adresa+"zadatak/"+id);
  }
  putZadatak(item):Observable<Zadatak>{
    return this.http.post<Zadatak>(this.adresa+"zadatak",item);
  }
  detailZadatak(id):Observable<Zadatak>{
    return this.http.get<Zadatak>(this.adresa+"zadatak/"+id);
  }
  changeZadatak(item):Observable<Zadatak>{
    return this.http.put<Zadatak>(this.adresa+"zadatak/"+item.id,item);
  }



}
