import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ZadatakComponent } from './zadatak/zadatak.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ZadatakDetaljComponent } from './zadatak-detalj/zadatak-detalj.component';


@NgModule({
  declarations: [
    AppComponent,
    ZadatakComponent,
    ZadatakDetaljComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
